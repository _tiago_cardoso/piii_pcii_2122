import UIKit
/*
var greeting = "Hello, playground"


var demo = "tteste"


var nome2 = 10
var _nome = 10
//var 0nome = 10
var π = 3.14
var 🐄 = "cão"

var d:Float = 14.9
var d2 = 10


var soma = d + Float(d2)
var soma2 = Int(d) + d2







var str = "Ola Mundo, "
var nome = "Gonçalo"
var ano = 2021

var resp = str + " " + nome + " em " + String(ano)

var resp2 = "Ola Mundo, \"\(nome.uppercased())\" em \(ano)"




var f = 10

Double(f).squareRoot()
sqrt(Double(f))


f *= 2





var t1 = ("Gonçalo", 2021, true)

t1.0
t1.1



var t2 = (nome:"Gonçalo", ano:2021, ativo:true)

t2.nome
t2.ano

t2.0
t2.1

var t3:(String, Int, Bool)

t3 = ("Gonçalo", 2021, true)

t3.0 = "Oscar"


var t4:(String, Int, Bool)

t4 = (nome:"Gonçalo", ano:2021, ativo:true)


var t5:(nome:String, ano:Int, ativo:Bool)

t5 = (nome:"Gonçalo", ano:2021, ativo:true)

t5.ano


var t6:(nome:String, ano:Int, ativo:Bool)


t6 = t3

t6.nome



var t7:(nome:String, ano:Int, ativo:Bool)

t7 = ("Gonçalo", 2021, true)


t7.nome



sqrt(16)



 f = 10
(-(-(-(-f)))) * 3
*/
 var aux = 101

if aux == 10 {
    
    print("ok")
    
}else if aux > 10 {
    
    print("ok > 10")
    
}else if aux < 10 {
    
    print("ok < 10")
    
}else {
    print("n ok")
}

aux = 10

switch aux{
case 10:
    print("10")
case 11:
    print("11")
case 12:
    print("12")
default:
    print("outro valor")
}

print("---------------------------")

for a in 0 ..< 20 {
    
    print(a)
    
}


var arr:[String]

arr = []

arr.append("Ola Mundo")


var foo = ["teste", "teste2", "teste3"]
arr.append(contentsOf: foo)

 
 arr.append(contentsOf: foo)
 arr.append(contentsOf: foo)
 arr.append(contentsOf: foo)
 arr.append(contentsOf: foo)
 arr.append(contentsOf: foo)
 arr.append(contentsOf: foo)

arr.append(contentsOf: foo)
arr.append(contentsOf: foo)
arr.append(contentsOf: foo)

arr.append("Ola Mundo")
arr.append("Ola Mundo")


print("---------------------------")

var x = arr.popLast()
arr.count
arr.capacity
arr.startIndex
arr.isEmpty

for a in arr{
    
    print(a)
    
}
print("---------------------------")

arr.forEach { a in
    print(a)

}
print("---------------------------")


var dict = ["key":"Value",
            "key2":"Value2",
            "key3":"Value3",
            "key4":"Value4"]


dict["key5"] = "Ola Mundo"

print(dict)

for a in dict{
    
    print("Key:\t\(a.key)")
    print("Value:\t\(a.value)")

}
print("---------------------------")


dict.forEach { (al1, casa) in
    
    print("Key:\t\(al1)")
    print("Value:\t\(casa)")
    
}

print("---------------------------")

var set_var:Set = [1,2,3,4,5]

set_var.capacity
set_var.count

set_var.insert(6)


set_var.capacity
set_var.count

set_var.insert(6)
set_var.insert(6)
set_var.insert(6)
set_var.insert(6)
set_var.insert(6)
set_var.insert(6)
set_var.insert(6)

set_var.capacity
set_var.count


for a in set_var{
    
    print(a)
}

print("---------------------------")

set_var.forEach { i in
    print(i)
}



print("---------------------------")
print(set_var)

set_var.remove(2)

print(set_var)


set_var.sorted()
arr.sort()


print("---------------------------")

var set_var1:Set = [0, 2, 4, 6, 8, 10]
var set_var2:Set = [0, 3, 6, 9]


set_var2.subtract(set_var1)

set_var2.union(set_var1)

set_var2.symmetricDifference(set_var1)

set_var2.intersection(set_var2)




func soma(nome:String) -> String{
    "Ola Mundo, \(nome)"
}

print(soma(nome: "Gonçalo"))

func soma1(num1:Int, num2:Int) -> Int{
    let soma = num1 + num2
    return soma
}


var x1 = soma1(num1: 19, num2: 92)

print(x1)



func soma3(_ num1:Int,com num2:Int) -> Int{
    let soma = num1 + num2
    return soma
}



_ = soma3(39, com: 3)



for _ in 0 ... 10{
    
    print("Ola ")
    
}
